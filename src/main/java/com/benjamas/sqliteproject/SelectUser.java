/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benjamas.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author sany
 */
public class SelectUser {
    
     public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM user");

            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");

                
                System.out.println("id = " + id);
                System.out.println("username = " + username);
                System.out.println("password = " + password);

                System.out.println();
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
    
}
