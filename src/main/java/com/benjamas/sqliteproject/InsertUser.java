/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benjamas.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author sany
 */
public class InsertUser {

    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            String sql = "INSERT INTO user (username,password) "
                    + "VALUES ('zai', 'password' );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (username,password) "
                    + "VALUES ('benja', 'password' );";
            stmt.executeUpdate(sql);

            conn.commit();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

}
